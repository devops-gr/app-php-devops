<!DOCTYPE html>
<html>
<body>

<?php
//
// A very simple PHP example that sends a HTTP POST to a remote site
//
echo "DevOps - PHP Application Front-End em 25/05/2022!!! <br/>";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://app-node-js-devops.herokuapp.com/juntar?");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "palavra1=devops&palavra2=test");

// In real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// Receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_close ($ch);

// Further processing ...
if (!preg_match('/^Set-Cookie:\s*([^;]*)/mi', $server_output)) 

{echo 'post successful'; }

else { echo 'not successful'; }
?>

</body>
</html>
